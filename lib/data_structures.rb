# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  # your code goes here
   sorted = arr.sort
   range_found = sorted.first - sorted.last
   range_found.abs
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  # your code goes here
  arr.sort == arr
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  # your code goes here
  str_low = str.downcase.split('')
  str_low_a = str_low.count('a')
  str_low_e = str_low.count('e')
  str_low_i = str_low.count('i')
  str_low_o = str_low.count('o')
  str_low_u = str_low.count('u')
  str_low_a + str_low_e + str_low_i + str_low_o + str_low_u
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  # your code goes here
  str_arr = str.downcase.split('')
  str_arr.delete('a')
  str_arr.delete('e')
  str_arr.delete('i')
  str_arr.delete('o')
  str_arr.delete('u')
  str_arr.join
end


# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  # your code goes here
  int_str_arr = int.to_s.split('')
  int_str_arr.sort.reverse
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  # your code goes here
  str_low = str.downcase.split('')
  str_low.uniq != str_low
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  # your code goes here
  area_code = arr[0..2].join
  office_code = arr[3..5].join
  line_number = arr[6..9].join
  phone_number = "(#{area_code}) #{office_code}-#{line_number}"
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  # your code goes here
  str_arr = str.split(',') # ['4','1','8']
  str_num = str_arr.join # '418'
  num_arr = str_num.split('').sort # ['1','4','8']
  num_range = num_arr.first.to_i - num_arr.last.to_i # 1 - 8
  num_range.abs
end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset=1) # [1,2,3,4]
  # your code goes here
  if offset > arr.length
    rotate_amount = offset / arr.length
    arr.drop(rotate_amount).concat(arr.take(rotate_amount))
  elsif offset > 0
    arr.drop(offset).concat(arr.take(offset)) # [2,3,4,1]
  else
    arr.drop(arr.length - offset.abs).concat(arr.take(arr.length - offset.abs))
  end
end
